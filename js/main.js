//change color of li when active
$('ul.navbar-nav > li').click(function (e) {
    $('ul.navbar-nav > li').removeClass('active');
    $(this).addClass('active');
});

//carousel intervals
$('.carousel').carousel({
    interval: 5000
});


//hiding mobile navbar onclick
var navMain = $(".navbar-collapse"); 

navMain.on("click", "a:not([data-toggle])", null, function () {
    navMain.collapse('hide');
});
